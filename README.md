# Docker images used for building CGS projects

## Updating to newer Rust versions

1. update Rust base image version in `Dockerfile`
2. git commit && git push
3. create a tag with Rust version number in the style `1.59` (change accordingly)
4. git push --tags

Images are built by the pipeline, tagged with the version number, and then pushed to the GitLab registry.
