#
# Create base image from https://hub.docker.com/_/rust/
#
FROM rust:1.59-slim AS rust-base
LABEL maintainer="Central Gaming System"

# add dependencies for building and running Rust tools
# update image and certificates
RUN apt-get update \
#    && apt-get -y dist-upgrade \
    && update-ca-certificates \
    && apt-get install -y \
        pkg-config libssl-dev \
    && rm -rf /var/lib/apt/lists/*

# 
# build additional Rust tools
#
FROM rust-base AS builder

# Rust tools
RUN cargo install cargo-make
# wasm-pack 0.10.0 does not currently build in Docker (https://github.com/rustwasm/wasm-pack/issues/1029)
RUN cargo install wasm-pack --version "=0.9.1"
RUN cargo install wasm-bindgen-cli --version "=0.2.79"

#
# create slimmer image by copying compiled tools from builder
#
FROM rust-base

# zip : required for packaging html5
# rest: required for building native
RUN apt-get update \
    && apt-get -y install \
        zip \
        pkg-config \
        libx11-dev \
        libasound2-dev \
        libudev-dev \
        mesa-vulkan-drivers \
        g++ \
        git \
    && rm -rf /var/lib/apt/lists/*

# add Rust WebAssembly target
RUN rustup target add wasm32-unknown-unknown

# copy over binaries and leave build files behind
COPY --from=builder /usr/local/cargo/bin/*make* /usr/local/cargo/bin/
COPY --from=builder /usr/local/cargo/bin/wasm* /usr/local/cargo/bin/

